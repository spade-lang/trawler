use async_trait::async_trait;

use color_eyre::{
    eyre::{bail, Context},
    Result,
};
use log::{debug, error};
use tokio::process::Command;

#[async_trait]
pub trait CommandExt {
    async fn success_or_report(&mut self) -> Result<()>;
    fn debug_command(&mut self) -> &mut Self;
}

#[async_trait]
impl CommandExt for Command {
    async fn success_or_report(&mut self) -> Result<()> {
        let output = self.output().await.context("Failed to run command")?;

        if !output.status.success() {
            error!("stdout:\n{}", String::from_utf8_lossy(&output.stdout));
            error!("stderr:\n{}", String::from_utf8_lossy(&output.stderr));
            bail!("Error when executing command")
        } else {
            Ok(())
        }
    }

    fn debug_command(&mut self) -> &mut Self {
        debug!("{:?}", self);
        self
    }
}
