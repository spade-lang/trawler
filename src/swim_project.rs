use serde::Deserialize;

#[derive(Deserialize)]
pub struct Empty {}

// We only care about the existene of these fields at this stage. therefore, we
// use an empty dummy struct
#[derive(Deserialize)]
#[allow(unused)]
pub struct PartialSwimProject {
    pub compiler: Option<Empty>,
    pub simulation: Option<Empty>,
    pub pnr: Option<Empty>,
    pub board: Option<Empty>,
}
