mod command_ext;
mod swim_project;

use std::collections::{HashMap, HashSet};
use std::path::Path;
use std::time::{Duration, Instant};

use camino::{Utf8Path, Utf8PathBuf};
use clap::Parser;
use color_eyre::{
    eyre::{bail, Context},
    Result,
};
use fern::colors::{Color, ColoredLevelConfig};
use futures::stream::FuturesUnordered;
use junit_report::{
    Duration as JUnitDuration, ReportBuilder, TestCase, TestCaseBuilder, TestSuite,
};
use log::{error, info};
use serde::Deserialize;
use tokio::process::Command;
use tokio_stream::StreamExt;

use command_ext::CommandExt;
use swim_project::PartialSwimProject;

const JUNIT_REPORT: &str = "junit-report.xml";

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
struct Config {
    pub libs: HashMap<String, Library>,
}

#[derive(Debug, Clone, Deserialize)]
#[serde(untagged)]
enum Library {
    Simple(String),
    Detailed {
        repository: String,
        directory: String,
    },
}

#[derive(Parser)]
struct Args {
    spade_git_ref: String,
    /// Output a JUnit-report.
    #[clap(long)]
    junit: bool,
    /// Only try these libraries.
    #[clap(long)]
    libraries: Vec<String>,
}

async fn clone_spade(git_ref: String) -> Result<()> {
    info!("Cloning spade");
    Command::new("git")
        .arg("clone")
        .arg("https://gitlab.com/spade-lang/spade.git")
        .args(["--depth", "1"])
        .arg("--recurse-submodules")
        .arg("--shallow-submodules")
        .arg("-b")
        .arg(&git_ref)
        .debug_command()
        .success_or_report()
        .await
}

struct RunResult {
    pub pass: bool,
    pub stdout: String,
    pub stderr: String,
    pub duration: Duration,
}

/// The result of testing a single library.
///
/// Fields are None for targets that don't exist in the library.
struct LibResult {
    pub build: RunResult,
    pub test: Option<RunResult>,
    pub pnr: Option<RunResult>,
}

#[derive(Default)]
struct Report {
    /// Maps targets to a list of failures for that target.
    pub failures: HashMap<String, Vec<String>>,
    pub successful: u32,
    pub junit_report: ReportBuilder,
}

impl Report {
    fn new() -> Self {
        Self::default()
    }

    fn add_lib_result(&mut self, name: &str, lib: LibResult, test_suite: &mut TestSuite) -> Result<()> {
        let LibResult { pnr, test, build } = lib;
        let tests = [(pnr, "pnr"), (test, "test"), (Some(build), "build")];
        // Filter out targets that don't exist.
        let tests: Vec<_> = tests
            .into_iter()
            .filter_map(|(test, target)| test.map(|test| (test, target)))
            .collect();
        if tests.iter().all(|(test, _)| test.pass) {
            self.successful += 1;
        }
        for (test, target) in tests {
            self.add_run_result(name, target, test, test_suite)?;
        }
        Ok(())
    }

    fn add_run_result(
        &mut self,
        name: &str,
        type_: &str,
        run: RunResult,
        test_suite: &mut TestSuite,
    ) -> Result<()> {
        let testcase_name = format!("{name} ({type_})");
        let duration = run.duration.try_into()?;
        if run.pass {
            test_suite.add_testcase(TestCase::success(&testcase_name, duration));
        } else {
            self.failures
                .entry(type_.to_string())
                .or_default()
                .push(name.to_string());
            error!("{name} test stdout:\n{}", run.stdout);
            error!("{name} test stderr:\n{}", run.stderr);
            test_suite.add_testcase(
                TestCaseBuilder::failure(&testcase_name, duration, "", "")
                    .set_system_out(&run.stdout)
                    .set_system_err(&run.stderr)
                    .testcase()
                    .to_owned(),
            );
        }

        Ok(())
    }
}

async fn run_swim(swim_command: &str, project_path: &Utf8Path, shared_compiler: &Path) -> Result<RunResult> {
    let start = Instant::now();
    let test_output = Command::new("swim")
        .arg("--override-compiler")
        .arg(shared_compiler)
        .arg(swim_command)
        .current_dir(project_path)
        .debug_command()
        .output()
        .await
        .with_context(|| format!("Failed to run swim {swim_command}"))?;
    let end = Instant::now();
    let duration = end - start;

    let stdout_path = project_path.join("stdout.log");
    let stderr_path = project_path.join("stderr.log");
    std::fs::write(&stdout_path, &test_output.stdout)
        .with_context(|| format!("Failed to write {stdout_path}"))?;
    std::fs::write(&stderr_path, &test_output.stdout)
        .with_context(|| format!("Failed to write {stderr_path}"))?;

    Ok(RunResult {
        pass: test_output.status.success(),
        stderr: String::from_utf8_lossy(&test_output.stderr).to_string(),
        stdout: String::from_utf8_lossy(&test_output.stdout).to_string(),
        duration,
    })
}

async fn inner_build_lib(
    lib_name: String,
    lib: Library,
    shared_compiler: &Path,
) -> Result<LibResult> {
    let (repository, directory) = match lib {
        Library::Simple(repository) => (repository, ".".to_string()),
        Library::Detailed {
            repository,
            directory,
        } => (repository, directory),
    };
    info!("Cloning {lib_name} from {repository}");
    Command::new("git")
        .arg("clone")
        .args(["--depth", "1"])
        .arg("--recurse-submodules")
        .arg("--shallow-submodules")
        .arg(&repository)
        .arg(&lib_name)
        .debug_command()
        .success_or_report()
        .await
        .with_context(|| format!("Failed to clone {lib_name} from {repository}"))?;

    // Try to figure out the spade compiler path that the subproject wants, and
    // change it to the one we cloned earlier
    let wd =
        Utf8PathBuf::from_path_buf(std::env::current_dir().context("Failed to get working dir")?)
            .unwrap();
    let project_path = wd.join(&lib_name).join(directory);
    let swim_config_path = project_path.join("swim.toml");
    let project_config = toml::from_str::<PartialSwimProject>(
        &tokio::fs::read_to_string(&swim_config_path)
            .await
            .with_context(|| format!("Failed to read swim.toml in {}", &swim_config_path))?,
    )
    .with_context(|| format!("Failed to parse {swim_config_path}"))?;

    info!("Running build for {lib_name}");
    let build = run_swim("build", &project_path, &shared_compiler).await?;

    let test = if project_config.simulation.is_some() {
        info!("Running tests for {lib_name}");
        Some(run_swim("test", &project_path, &shared_compiler).await?)
    } else {
        info!("{lib_name} does not have a simulation section");
        None
    };
    // NOTE: We assume that [board] guarantees that we can do pnr.
    let pnr = if project_config.pnr.is_some() || project_config.board.is_some() {
        info!("Running pnr for {lib_name}");
        Some(run_swim("pnr", &project_path, &shared_compiler).await?)
    } else {
        info!("{lib_name} does not have a pnr section");
        None
    };

    Ok(LibResult { build, test, pnr })
}

async fn build_lib(
    lib_name: String,
    lib: Library,
    shared_compiler: &Path,
) -> (String, Result<LibResult>) {
    (
        lib_name.clone(),
        inner_build_lib(lib_name.clone(), lib, shared_compiler).await,
    )
}

fn setup_logging() -> Result<()> {
    let colors = ColoredLevelConfig::new()
        .error(Color::Red)
        .warn(Color::Yellow)
        .info(Color::Green)
        .debug(Color::Blue)
        .trace(Color::White);

    let stdout_config = fern::Dispatch::new()
        .level(log::LevelFilter::Debug)
        .format(move |out, message, record| {
            out.finish(format_args!(
                "[{}] {}",
                colors.color(record.level()),
                message
            ))
        })
        .chain(std::io::stdout());

    fern::Dispatch::new().chain(stdout_config).apply()?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    color_eyre::install()?;
    setup_logging()?;

    let args = Args::parse();

    let lib_file = tokio::fs::read_to_string("trawl.toml")
        .await
        .context("Failed to read trawl.toml")?;
    let config = toml::from_str::<Config>(&lib_file).context("Failed to parse trawl.toml")?;

    if Utf8PathBuf::from("work").exists() {
        tokio::fs::remove_dir_all("work")
            .await
            .context("Failed to remove work dir")?;
    }
    tokio::fs::create_dir_all("work")
        .await
        .context("Failed to create directory 'work'")?;

    std::env::set_current_dir("work").context("Failed to cd into 'work'")?;

    clone_spade(args.spade_git_ref).await?;
    let spade_compiler = std::env::current_dir()
        .context("Failed to get working_dir")?
        .join("spade");

    let mut build_fut = FuturesUnordered::new();

    let libs: HashSet<_> = args.libraries.iter().collect();
    for (lib_name, lib) in config.libs.clone() {
        if libs.is_empty() || libs.contains(&lib_name) {
            build_fut.push(build_lib(lib_name, lib, &spade_compiler))
        }
    }

    let mut results = vec![];
    while let Some(result) = build_fut.next().await {
        results.push(result);
    }

    let num_results = results.len();
    let mut report = Report::new();

    for (name, result) in results {
        let mut test_suite = TestSuite::new(&name);
        match result {
            Ok(result) => {
                report.add_lib_result(&name, result, &mut test_suite)?;
            },
            Err(e) => {
                error!("{name}: {e}");
                error!("{e:?}");
                test_suite.add_testcase(TestCase::error(
                    &name,
                    JUnitDuration::ZERO,
                    &e.to_string(),
                    "",
                ));
            },
        }
        report.junit_report.add_testsuite(test_suite);
    }

    for (type_, failures) in &report.failures {
        if !failures.is_empty() {
            error!("{type_} failures");
            for lib in failures {
                error!("  {lib}");
            }
        }
    }

    info!(
        "{}/{} libs passed",
        report.successful,
        num_results,
    );

    if args.junit {
        info!(r#"Storing JUnit report to "{JUNIT_REPORT}""#);
        let mut junit_file = std::fs::File::create(JUNIT_REPORT)
            .context(format!(r#"Failed to create "{JUNIT_REPORT}""#))?;
        report
            .junit_report
            .build()
            .write_xml(&mut junit_file)
            .context(format!(r#"Failed to write "{JUNIT_REPORT}""#))?;
    }

    let all_success = report.failures.values().all(|failures| failures.is_empty());
    if all_success {
        Ok(())
    } else {
        bail!("Some libs failed to build");
    }
}
