# Trawler

Runs swim tests and pnr on a list of swim projects to ensure that they still
build in updated spade versions. Primarily for use in CI.

Collected statistics can be found at https://trawler.spade-lang.org.

**Warning**: This runs somewhat arbitrary code, especially arbitrary python in
tests. You should probably run this in a sandboxed environment.
